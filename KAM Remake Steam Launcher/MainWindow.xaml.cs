﻿namespace Rewor.Steam.KAM
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Markup;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Func<DrawingImage> iconOk = () => (DrawingImage) App.Current.Resources["StatusAnnotations_Complete_and_ok_32xLG_color"];
            Func<DrawingImage> iconErr = () => (DrawingImage) App.Current.Resources["StatusAnnotations_Critical_32xLG_color"];

            Action<TextBlock, InvalidStateException> invalidStateMessage = (t, ex) =>
            {
                t.Text = ex.Message;
                if (ex.LinkUrl != null)
                {
                    var hyperlink = new Hyperlink(new Run(ex.LinkText));
                    hyperlink.NavigateUri = ex.LinkUrl;
                    hyperlink.RequestNavigate += (o, a) =>
                    {
                        Process.Start(new ProcessStartInfo(a.Uri.AbsoluteUri));
                        a.Handled = true;
                    };

                    t.Inlines.Add(new Run(" "));
                    t.Inlines.Add(hyperlink);
                }
            };

            /* Steam Path */
            try
            {
                steamPathText.Text = KAMPaths.KAMSteamPath;
                steamPathIcon.Source = iconOk();
            }
            catch (InvalidStateException ex)
            {
                invalidStateMessage(steamPathText, ex);
                steamPathIcon.Source = iconErr();
            }

            /* Remake Path */
            try
            {
                remakePathText.Text = KAMPaths.KAMRemakePath;
                remakePathIcon.Source = iconOk();
            }
            catch (InvalidStateException ex)
            {
                invalidStateMessage(remakePathText, ex);
                remakePathIcon.Source = iconErr();
            }

            /* Install button */
            CheckInstalled();
        }

        private void CheckInstalled()
        {
            installButton.IsEnabled = !EntryPoint.Installed;
            installOkIcon.Visibility = EntryPoint.Installed ? Visibility.Visible : Visibility.Collapsed;
            installUacIcon.Visibility = EntryPoint.Installed || EntryPoint.IsElevated ? Visibility.Collapsed : Visibility.Visible;
            installText.Text = EntryPoint.Installed ? Properties.Resources.LauncherInstalled : Properties.Resources.LauncherInstall;
        }

        private void installButton_Click(object sender, RoutedEventArgs e)
        {
            EntryPoint.Install();
            CheckInstalled();
        }
    }
}
