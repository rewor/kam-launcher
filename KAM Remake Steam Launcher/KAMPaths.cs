﻿namespace Rewor.Steam.KAM
{
    using System;
    using System.IO;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using Microsoft.Win32;
    using SR = Properties.Resources;

    class KAMPaths
    {

        public static string KAMSteamPath
        {
            get
            {
                var steamreg = Registry.CurrentUser.OpenSubKey(@"Software\Valve\Steam");
                var steampath = steamreg.GetValue("SteamPath") as string;
                if (steampath == null)
                {
                    throw new InvalidStateException(SR.SteamNotInstalled,
                        SR.SteamNotInstalled_Url,
                        SR.SteamNotInstalled_Link);
                }

                var steamcfg = Path.Combine(steampath, "config", "config.vdf");
                if (!File.Exists(steamcfg))
                {
                    throw new InvalidStateException(SR.SteamNotInstalled, 
                        SR.SteamNotInstalled_Url, 
                        SR.SteamNotInstalled_Link);
                }

                dynamic configvdf = VDF.OpenFile(steamcfg);
                dynamic configapps = configvdf.Software.Valve.Steam.apps;
                dynamic configkam = configapps[253900];
                if (configkam == null)
                {
                    throw new InvalidStateException(SR.SteamKAMNotOwned, 
                        SR.SteamKAMNotOwned_Url, 
                        SR.SteamKAMNotOwned_Link);
                }

                var installdir = (string) configkam.installdir;
                if (installdir == null || !Directory.Exists(installdir))
                {
                    throw new InvalidStateException(SR.SteamKAMNotInstalled, 
                        SR.SteamKAMNotInstalled_Url, 
                        SR.SteamKAMNotInstalled_Link);
                }

                return installdir;
            }
        }

        private static readonly string RemakeId = "FDE049C8-E4B2-4EB5-A534-CF5C581F5D32";

        public static string KAMRemakePath
        {
            get
            {
                var uninstreg = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
                var remakereg = uninstreg.OpenSubKey(string.Format("{{{0}}}_is1", RemakeId));
                if (remakereg == null)
                {
                    throw new InvalidStateException(SR.RemakeKAMNotInstalled,
                        SR.RemakeKAMNotInstalled_Url,
                        SR.RemakeKAMNotInstalled_Link);
                }

                var installdir = remakereg.GetValue("InstallLocation") as string;
                return installdir;
            }
        }

    }

    class InvalidStateException : Exception
    {
        public Uri LinkUrl { get; private set; }
        public string LinkText { get; private set; }

        public InvalidStateException(string message, string url, string link)
            : base(message)
        {
            LinkUrl = new Uri(url);
            LinkText = link;
        }
    }
}
