﻿namespace Rewor.Steam.KAM
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Dynamic;
    using System.IO;
    using System.Text;

    class VDF : DynamicObject
    {
        private VDFEntry node;

        public static VDF OpenFile(string path)
        {
            VDFEntry root = new VDFEntry();
            using (var stream = File.OpenRead(path))
            using (var reader = new StreamReader(stream))
            {
                bool quote = false;

                VDFEntry entry = root;
                VDFEntry prevroot = null;
                StringBuilder value = new StringBuilder();

                Func<VDFEntry, VDFEntry> nextEntry = (newRoot) =>
                {
                    prevroot = newRoot;
                    entry = new VDFEntry();
                    entry.Parent = prevroot;
                    return entry;
                };

                while (!reader.EndOfStream)
                {
                    var ch = (char) reader.Read();
                    switch (ch)
                    {
                        case '{':
                            entry.children = new List<VDFEntry>();
                            entry = nextEntry(entry);
                            break;

                        case '}':
                            entry = nextEntry(prevroot.Parent);
                            break;

                        case '"':
                            quote = !quote;
                            if (!quote)
                            {
                                if (entry.Name == null)
                                {
                                    entry.Name = value.ToString();
                                    if (prevroot != null)
                                        prevroot.children.Add(entry);
                                }
                                else if (entry.Value == null)
                                {
                                    entry.Value = value.ToString();
                                    entry = nextEntry(prevroot);
                                }
                                else
                                    throw new InvalidDataException();

                                value.Clear();
                            }
                            break;

                        //case '/':
                        //    break;

                        case '\\':
                            var nch = (char) reader.Read();
                            switch (nch)
                            {
                                case 'n': ch = '\n'; break;
                                case 't': ch = '\t'; break;
                                case '\\': ch = '\\'; break;
                                case '"': ch = '"'; break;

                                default:
                                    throw new InvalidDataException("Unknown escape sequence: \\" + nch);
                            }
                            goto default;

                        default:
                            if (char.IsWhiteSpace(ch) && !quote)
                                break;

                            value.Append(ch);
                            break;
                    }
                }
            }

            return new VDF(root);
        }

        private VDF(VDFEntry root)
        {
            node = root;
        }

        private VDF FindChild(string name)
        {
            foreach (var ch in node.children)
            {
                if (ch.Name.Equals(name))
                    return new VDF(ch);
            }

            return null;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = FindChild(binder.Name);
            return result != null;
        }

        public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
        {
            Contract.Assert(indexes.Length == 1);

            result = FindChild(indexes[0].ToString());
            return result != null;
        }

        public override bool TryConvert(ConvertBinder binder, out object result)
        {
            if (binder.Type == typeof(string))
            {
                result = node.Value;
                return true;
            }

            return base.TryConvert(binder, out result);
        }
    }

    class VDFEntry
    {
        internal VDFEntry Parent { get; set; }
        internal string Name { get; set; }
        internal string Value { get; set; }
        internal List<VDFEntry> children;
    }
}
