﻿namespace Rewor.Steam.KAM
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Security.Principal;
    using System.Windows;
    using System.Linq;

    class EntryPoint
    {
        /* How to get configuration from steam?
         * https://steamdb.info/app/253900/config/ */
        private static readonly string ExecutableName = "KM_TPR.exe";

        [STAThread]
        public static void Main()
        {
            try
            {
                if (RunningFromSteam)
                {
                    string remake = Path.Combine(KAMPaths.KAMRemakePath, "KaM_Remake.exe");
                    Process.Start(new ProcessStartInfo(remake));
                    return;
                }

                if (CmdLine.CommandLine.Tokenize().Any(c => c.Command == "install"))
                {
                    Install();
                    return;
                }
            }
            catch (InvalidStateException ex)
            {
                /* no-op */
            }

            App.Main();
        }

        public static bool Installed { get { return (Checksum(ActualPath) == Checksum(ExpectedPath)); } }
        private static bool RunningFromSteam { get { return (NormalizePath(ActualPath) == NormalizePath(ExpectedPath)); } }
        public static bool IsElevated
        {
            get
            {
                WindowsIdentity id = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(id);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
        }

        private static string ExpectedPath { get { return Path.Combine(KAMPaths.KAMSteamPath, ExecutableName); } }
        private static string ActualPath { get { return Assembly.GetExecutingAssembly().Location; } }

        public static void Install()
        {
            if (!IsElevated)
            {
                try
                {
                    var psi = new ProcessStartInfo(ActualPath, "/install");
                    psi.Verb = "runas";

                    var process = new Process();
                    process.EnableRaisingEvents = true;
                    process.StartInfo = psi;
                    process.Start();
                    process.WaitForExit();
                }
                catch (Win32Exception ex)
                {
                    /* no-op: canceled by user */
                }
            }
            else
            {
                File.Copy(ActualPath, ExpectedPath, true);
            }
        }

        private static string Checksum(string filePath)
        {
            using (var stream = new BufferedStream(File.OpenRead(filePath), 100000))
            {
                byte[] hash = new MD5CryptoServiceProvider().ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }

        private static string NormalizePath(string path)
        {
            return Path.GetFullPath(new Uri(path).LocalPath)
                       .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                       .ToUpperInvariant();
        }
    }
}
